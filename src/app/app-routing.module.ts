import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlantComponent } from './component/dashboard/plant/plant.component';

const routes: Routes = [
  {
    path : 'dashboard', children : [
      {path : '', redirectTo: 'plant', pathMatch: 'full'},
      {path : 'plant', component: PlantComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
