import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-plant',
  templateUrl: './plant.component.html',
  styleUrls: ['./plant.component.scss']
})
export class PlantComponent implements OnInit {

  form !: FormGroup;
  title !: string;
  name !: string;
  // mobile !: string;
  email !: string;

  constructor(
    private fb : FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: ['', []],
      name: ['', [Validators.required]],
      // mobile: ['', [Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.email ]]
    })
  }

  registerPlant() {
    console.log(this.form.controls)
  }
}
